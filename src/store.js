import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from "./reducers/rootReducer";

// crea el store conectando los reducers combinados en el root
export default function configureStore(initialState = {}) {
    return createStore(
        rootReducer,
        applyMiddleware(thunk)
    )
}
