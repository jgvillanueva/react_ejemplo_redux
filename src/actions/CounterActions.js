// action creators de la app

import * as types from './ActionTypes';

export function increment() {
    return {
        type: types.INCREMENT
    }
}
export function decrement() {
    return {
        type: types.DECREMENT
    }
}
