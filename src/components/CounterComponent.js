import React, { Component } from 'react';
import {connect} from 'react-redux';
import {increment, decrement} from '../actions/CounterActions';


class CounterComponent extends Component {


    render() {
        return (
            <div className="CounterComponent">
                <button onClick={this.props.increment}>Aumentar</button>
                {this.props.count}
                <button onClick={this.props.decrement}>Disminuir</button>
            </div>
        );
    }
}


const mapStateToProps = state => ({
    count: state.counter.count
})

const mapDispatchToProps = (dispatch) => {
    return {
        increment: () => {
            dispatch(increment());
        },
        decrement: () => {
            dispatch(decrement());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CounterComponent)
