// listado de reducers

import * as types from '../actions/ActionTypes';

// estado inicial de la app
const initialState = {
    count: 0,
}

export default function reducer(state = initialState, action = {}) {
    // dependiendo de la acción realiza una modificación sobre el estado
    switch(action.type) {
        case types.INCREMENT:
            return {
                ...state,
                count: state.count + 1,
            }
            break;
        case types.DECREMENT:
            return {
                ...state,
                count: state.count - 1,
            }
            break;
        default:
            return state
    }
}
